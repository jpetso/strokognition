/*
    SPDX-FileCopyrightText: 2024 Jin Liu <m.liu.jin@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "strokognitioneffect.h"

namespace KWin
{

KWIN_EFFECT_FACTORY_SUPPORTED(StrokognitionEffect, "metadata.json", return StrokognitionEffect::supported();)

} // namespace KWin

#include "main.moc"
