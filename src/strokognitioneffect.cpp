/*
    SPDX-FileCopyrightText: 2024 Jin Liu <m.liu.jin@gmail.com>
    SPDX-FileCopyrightText: 2024 Jakob Petsovits <jpetso@petsovits.com>
    SPDX-FileCopyrightText: 2023 Daniel Kondor <kondor.dani@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later AND MIT
*/

#include "strokognitioneffect.h"

// generated
#include <strokognitionconfig.h>

// KWin
#include <core/inputdevice.h>
#include <core/rendertarget.h>
#include <core/renderviewport.h>
#include <cursor.h>
#include <effect/effecthandler.h>
#include <input_event.h>
#include <opengl/gltexture.h>
#include <opengl/glutils.h>
#include <pointer_input.h>

// Qt
#include <QPainter>

// std
#include <chrono> // std::milliseconds
#include <cmath> // std::hypot

using namespace std::chrono_literals;

namespace KWin
{

const int TEXTURE_PADDING = 10;

StrokognitionEffect::StrokognitionEffect()
{
    input()->prependInputEventFilter(this);

    m_inAnimation.setEasingCurve(QEasingCurve::InOutCubic);
    connect(&m_inAnimation, &QVariantAnimation::valueChanged, this, [this]() {
        m_animationValue = m_inAnimation.currentValue().toReal();
        effects->addRepaintFull();
    });

    m_buttonlessStrokeTimer.setSingleShot(true);
    connect(&m_buttonlessStrokeTimer, &QTimer::timeout, this, &StrokognitionEffect::endStroke);

    m_outAnimation.setEasingCurve(QEasingCurve::InOutCubic);
    connect(&m_outAnimation, &QVariantAnimation::valueChanged, this, [this]() {
        m_animationValue = m_outAnimation.currentValue().toReal();
        effects->addRepaintFull();
    });

    StrokognitionConfig::instance(effects->config());
    reconfigure(ReconfigureAll);
}

StrokognitionEffect::~StrokognitionEffect()
{
}

bool StrokognitionEffect::supported()
{
    return
#ifdef KWIN_6_1
        effects->isWayland() &&
#endif
        effects->isOpenGLCompositing();
}

void StrokognitionEffect::reconfigure(ReconfigureFlags flags)
{
    Q_UNUSED(flags)

    StrokognitionConfig::self()->read();

    m_startButtonlessStrokeTimeout = std::chrono::milliseconds(StrokognitionConfig::startButtonlessStrokeTimeoutMsec());
    m_endButtonlessStrokeTimeout = std::chrono::milliseconds(StrokognitionConfig::endButtonlessStrokeTimeoutMsec());
    if (m_endButtonlessStrokeTimeout == 0ms) {
        m_endButtonlessStrokeTimeout = m_startButtonlessStrokeTimeout;
    }

    m_animationTime = animationTime(std::chrono::milliseconds(StrokognitionConfig::animationTime()));
    m_spotlightRadius = StrokognitionConfig::spotlightRadius();

    // example strokes, until we get them through some other means (config, D-Bus, etc.)
    std::vector<QPointF> rightwardPoints;
    std::vector<QPointF> leftwardPoints;
    for (int i = 0; i < 9; ++i) {
        rightwardPoints.push_back(QPointF(i / 8.0, 0.5));
        leftwardPoints.push_back(QPointF(1.0 - (i / 8.0), 0.5));
    }
    m_availableStrokes[1] = Stroke(rightwardPoints);
    m_availableStrokes[3] = Stroke(leftwardPoints);

    const int imageSize = (m_spotlightRadius + TEXTURE_PADDING) * 2;
    QImage image(imageSize, imageSize, QImage::Format_ARGB32);
    QPainter painter(&image);

    painter.fillRect(image.rect(), QColor(0, 0, 0));

    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(QColor(255, 255, 255));
    painter.setBrush(brush);
    painter.setRenderHint(QPainter::RenderHint::Antialiasing);
    painter.drawEllipse(image.rect().center(), m_spotlightRadius, m_spotlightRadius);

    auto style = StrokognitionConfig::spotlightStyle();
    if (style == StrokognitionConfig::EnumSpotlightStyle::Crosshair) {
        QPen pen(QColor(0, 0, 0));
        painter.setPen(pen);
        painter.setRenderHint(QPainter::RenderHint::Antialiasing, false);
        painter.drawLine(image.rect().center().x(), 0, image.rect().center().x(), imageSize);
        painter.drawLine(0, image.rect().center().y(), imageSize, image.rect().center().y());
    }

    m_strokognitionTexture.reset();
    m_strokognitionTexture = GLTexture::upload(image);
    m_strokognitionTexture->setWrapMode(GL_CLAMP_TO_EDGE);
    m_strokognitionTexture->setFilter(GL_LINEAR);
}

bool StrokognitionEffect::isActive() const
{
    return m_activeGrabDevice != nullptr || m_outAnimation.state() == QVariantAnimation::Running;
}

bool StrokognitionEffect::pointerEvent(MouseEvent *event, quint32 nativeButton)
{
    switch (event->type()) {
    case QEvent::MouseButtonPress:
        return mouseButtonPressEvent(event, nativeButton);
    case QEvent::MouseButtonRelease:
        return mouseButtonReleaseEvent(event, nativeButton);
    case QEvent::MouseMove:
        return mouseMoveEvent(event);
    default:
        return false;
    }
}

bool StrokognitionEffect::mouseButtonPressEvent(MouseEvent *event, quint32 nativeButton)
{
    if (!event->device()) {
        return false;
    }

    auto grabIt = m_buttonGrabs.find(event->device());
    if (grabIt != m_buttonGrabs.end()) {
        // Once we've started a button grab, pressing another button will cancel a possible stroke
        if (nativeButton != grabIt->nativeButton) {
            if (m_activeGrabDevice == event->device()) {
                endStroke();
            } else {
                releaseButtonGrab(event->device(), *grabIt);
            }
        }
        // Also pass through an emulated activation button press that never turned "active"
        return false;
    }

    // Multiple input devices can be grabbed/inhibited, but once any of them moves far enough,
    // we enter "active" stroke recognition and discard any other attempts at starting a new one
    if (m_activeGrabDevice != nullptr) {
        return false;
    }

    // Consider looking for strokes when *only* the activation button is pressed
    if (event->button() == m_activationButton && (event->buttons() & ~m_activationButton) == 0) {
        m_buttonGrabs.insert(event->device(),
                             ButtonGrab{
                                 .points = {event->position()},
                                 .nativeButton = nativeButton,
                                 .lastTimestamp = event->timestamp(),
                             });
        QObject::connect(event->device(), &QObject::destroyed, this, &StrokognitionEffect::onInputDeviceDestroyed, Qt::UniqueConnection);
        return true; // inhibit button event
    }

    return false;
}

void StrokognitionEffect::onInputDeviceDestroyed(QObject *device)
{
    if (m_activeGrabDevice == device) {
        m_buttonlessStrokeTimer.stop();
        m_activeGrabDevice = nullptr;
        startOutAnimation();
    }
    m_buttonGrabs.remove(static_cast<InputDevice *>(device));
}

bool StrokognitionEffect::mouseButtonReleaseEvent(MouseEvent *event, quint32 nativeButton)
{
    if (!event->device()) {
        return false;
    }

    auto grabIt = m_buttonGrabs.find(event->device());
    if (grabIt == m_buttonGrabs.end()) {
        return false;
    }

    if (nativeButton == grabIt->nativeButton) {
        if (grabIt->releasing) {
            // Emulated button release after ending stroke recognition
            QObject::disconnect(event->device(), &QObject::destroyed, this, &StrokognitionEffect::onInputDeviceDestroyed);
            m_buttonGrabs.erase(grabIt);
            return false;
        }
        grabIt->lastTimestamp = event->timestamp();

        if (m_startButtonlessStrokeTimeout > 0ms && !m_activeGrabDevice) {
            m_buttonlessStrokeTimer.stop();
            m_buttonlessStrokeTimer.setInterval(m_startButtonlessStrokeTimeout);
            m_buttonlessStrokeTimer.start();
        } else {
            endStroke();
        }
        return true; // inhibit button event
    }

    return false;
}

bool StrokognitionEffect::mouseMoveEvent(MouseEvent *event)
{
    if (m_activeGrabDevice != nullptr && m_activeGrabDevice != event->device()) {
        return false;
    }

    auto grabIt = m_buttonGrabs.find(event->device());
    if (grabIt == m_buttonGrabs.end()) {
        return false;
    }

    // Start stroke recognition if the mouse moves far enough from the button-pressed starting point
    const auto delta = grabIt->points[0] - event->position();
    const auto distance = std::hypot(delta.x(), delta.y());

    if (m_activeGrabDevice == nullptr && distance >= m_activationDistance) {
        m_activeGrabDevice = event->device();
        releaseInactiveButtonGrabs();
        qDebug() << "Starting stroke recognition -" << m_activeGrabDevice;

        startInAnimation();
    }

    // Track stroke points in order to draw and match them later
    grabIt->points.push_back(event->position());

    if (m_activeGrabDevice != nullptr) {
        effects->addRepaintFull();
    }

    // Extend the duration of buttonless stroke timeouts if that's what we started with
    if (m_buttonlessStrokeTimer.isActive()) {
        m_buttonlessStrokeTimer.stop();
        m_buttonlessStrokeTimer.setInterval(m_endButtonlessStrokeTimeout);
        m_buttonlessStrokeTimer.start();

        // Timestamp for emulating a mouse click that doesn't get to "active" stroke recognition stage
        grabIt->lastTimestamp = event->timestamp();
    }

    return false;
}

void StrokognitionEffect::endStroke()
{
    m_buttonlessStrokeTimer.stop();
    releaseInactiveButtonGrabs();

    if (m_activeGrabDevice == nullptr) {
        return;
    }
    qDebug() << "Ending stroke recognition -" << m_activeGrabDevice << "-" << m_buttonGrabs[m_activeGrabDevice].points.size() << "points";

    // Find the best stroke match among candidates
    Stroke capturedStroke(m_buttonGrabs[m_activeGrabDevice].points);
    auto bestStrokeIt = m_availableStrokes.cend();
    double bestStrokeScore = 0.0;

    for (auto it = m_availableStrokes.cbegin(); it != m_availableStrokes.cend(); ++it) {
        const Stroke &candidate = it->second;
        double score;
        if (!Stroke::compare(capturedStroke, candidate, score) || score < Stroke::min_matching_score()) {
            continue;
        }
        if (score > bestStrokeScore) {
            bestStrokeScore = score;
            bestStrokeIt = it;
        }
    }

    if (bestStrokeIt != m_availableStrokes.cend()) {
        auto strokeId = bestStrokeIt->first;
        qDebug() << "Matched stroke id" << strokeId << "score:" << bestStrokeScore;
    } else {
        qDebug() << "No stroke matches";
    }

    // Drop any remaining data we had about the gesture
    QObject::disconnect(m_activeGrabDevice, &QObject::destroyed, this, &StrokognitionEffect::onInputDeviceDestroyed);
    m_buttonGrabs.remove(m_activeGrabDevice);
    m_activeGrabDevice = nullptr;

    startOutAnimation();
}

void StrokognitionEffect::releaseInactiveButtonGrabs()
{
    for (auto grabIt = m_buttonGrabs.begin(); grabIt != m_buttonGrabs.end(); ++grabIt) {
        if (grabIt.key() != m_activeGrabDevice) {
            releaseButtonGrab(grabIt.key(), *grabIt);
        }
    }
}

void StrokognitionEffect::releaseButtonGrab(InputDevice *device, ButtonGrab &grab)
{
    grab.releasing = true;

    QTimer::singleShot(0, this, [this, dev = QPointer(device), button = grab.nativeButton, ts = grab.lastTimestamp] {
        if (!dev) {
            return;
        }
        // FIXME: processButton() is marked as @internal within KWin, do this with publicly available API
        input()->pointer()->processButton(button, InputRedirection::PointerButtonPressed, ts, dev);
        input()->pointer()->processButton(button, InputRedirection::PointerButtonReleased, ts, dev);

        // Keep m_buttonGrabs[device] for a little longer to avoid infinite press/release loops.
        // We'll erase it in mouseButtonReleaseEvent().
    });
}

void StrokognitionEffect::startInAnimation()
{
    updateMaxScale();
    qreal start = 1.0;
    if (m_outAnimation.state() == QVariantAnimation::Running) {
        start = m_outAnimation.currentValue().toReal();
        m_outAnimation.stop();
    }
    m_inAnimation.setStartValue(start);
    m_inAnimation.setEndValue(0.0);
    m_inAnimation.setDuration(m_animationTime * start);
    m_inAnimation.start();
}

void StrokognitionEffect::startOutAnimation()
{
    updateMaxScale();
    m_outAnimation.setStartValue(0.0);
    m_outAnimation.setEndValue(1.0);
    m_outAnimation.setDuration(m_animationTime);
    m_outAnimation.start();
}

void StrokognitionEffect::paintScreen(const RenderTarget &renderTarget, const RenderViewport &viewport, int mask, const QRegion &region, Output *screen)
{
    effects->paintScreen(renderTarget, viewport, mask, region, screen);

    QPointF center = cursorPos();

    if (screen != effects->screenAt(center.toPoint())) {
        return;
    }

    QRectF screenGeometry = screen->geometry();

    center -= screenGeometry.topLeft();

    qreal scale = 1 / (m_animationValue * m_maxScale + 1 - m_animationValue);
    QRectF source = QRectF(-center.x() * scale + TEXTURE_PADDING + m_spotlightRadius,
                           -center.y() * scale + TEXTURE_PADDING + m_spotlightRadius,
                           screenGeometry.width() * scale,
                           screenGeometry.height() * scale);
    QRectF fullscreen(screenGeometry.topLeft() * viewport.scale(), screenGeometry.size() * viewport.scale());

    auto shader = ShaderManager::instance()->pushShader(ShaderTrait::MapTexture | ShaderTrait::TransformColorspace);
    shader->setColorspaceUniformsFromSRGB(renderTarget.colorDescription());
    QMatrix4x4 mvp = viewport.projectionMatrix();
    mvp.translate(fullscreen.x(), fullscreen.y());
    shader->setUniform(GLShader::Mat4Uniform::ModelViewProjectionMatrix, mvp);

    const bool clipping = region != infiniteRegion();
    const QRegion clipRegion = clipping ? viewport.mapToRenderTarget(region) : infiniteRegion();

    if (clipping) {
        glEnable(GL_SCISSOR_TEST);
    }
    glEnable(GL_BLEND);
    glBlendColor(0.0f, 0.0f, 0.0f, 0.5f * (1.0f - m_animationValue));
    glBlendFunc(GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
    m_strokognitionTexture->render(source, clipRegion, fullscreen.size(), clipping);
    glDisable(GL_BLEND);
    if (clipping) {
        glDisable(GL_SCISSOR_TEST);
    }

    ShaderManager::instance()->popShader();
}

void StrokognitionEffect::updateMaxScale()
{
    QPointF center = cursorPos();
    QRectF screenGeometry = effects->screenAt(center.toPoint())->geometry();
    center -= screenGeometry.topLeft();
    qreal x = qMax(center.x(), screenGeometry.width() - center.x());
    qreal y = qMax(center.y(), screenGeometry.height() - center.y());
    qreal furthestDistanceToScreenCorner = qSqrt(x * x + y * y);
    m_maxScale = furthestDistanceToScreenCorner / m_spotlightRadius;
}

} // namespace KWin

#include "moc_strokognitioneffect.cpp"
